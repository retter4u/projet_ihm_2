package main;
import java.awt.*;
import javax.swing.*;

import Controler.PanneauChoix;
import Vue.VueDessin;


public class Fenetre extends JPanel{
	
	
	/**
	 * Attributs
	 */
	private static final long serialVersionUID = 1L;
	private VueDessin vueDessin;
	private PanneauChoix panneauChoix;

	/**
	 * Constructeur
	 */
	public Fenetre() {
		vueDessin= new VueDessin();
		panneauChoix = new PanneauChoix(vueDessin);
		this.setLayout(new BorderLayout());
		this.add(panneauChoix, BorderLayout.NORTH);
		this.add(vueDessin, BorderLayout.CENTER);
	}
	
	
	/**
	 * Affichage de l interface graphique
	 * @param args
	 */
	public static void main(String []args) {
		Fenetre f= new Fenetre();
		JFrame fenetre = new JFrame("RETTER_BOURJIJ");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setPreferredSize(new Dimension(1000,1000));
		fenetre.setContentPane(f);
		fenetre.pack();
		fenetre.setVisible(true);
	}
	
}
