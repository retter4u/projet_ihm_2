import java.awt.*;
import javax.swing.*;


public class Fenetre extends JPanel{

	private static final long serialVersionUID = 1L;
	private VueDessin vueDessin;
	private PanneauChoix panneauChoix;

	public Fenetre() {
		panneauChoix = new PanneauChoix(vueDessin);
		vueDessin= new VueDessin();
		this.setLayout(new BorderLayout());
		this.add(panneauChoix, BorderLayout.NORTH);
		this.add(vueDessin, BorderLayout.CENTER);
	}
	
	public static void main(String []args) {
		Fenetre f= new Fenetre();
		JFrame fenetre = new JFrame("projet");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setPreferredSize(new Dimension(1000,1000));
		fenetre.setContentPane(f);
		fenetre.pack();
		fenetre.setVisible(true);
	}
	
}