import java.util.*;
import javax.swing.*;
import java.awt.*;
public class VueDessin extends JPanel implements Observer{

	private static final long serialVersionUID = 1L;

	public VueDessin() {
		
	}

	public void update(Observable o, Object arg) {
		
		repaint();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
	}
	
	
	
}