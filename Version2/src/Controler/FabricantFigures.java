package Controler;
import java.awt.event.*;
import Model.*;
import Vue.*;

public class FabricantFigures implements MouseListener {


	/**
	 * Attributs
	 * nb_points_cliques nombre de points cliques avec la souris
	 * points_cliques tableau des points cliques avec la souris
	 * figure FigureColoree que l utilisateur veut construire 
	 */
	private int nb_points_cliques;
	private Model.Point[] points_cliques;
	private FigureColoree figure;
	
	/**
	 * Constructeur
	 * @param fg Figure que l utilisateur veut creer
	 */
	public FabricantFigures(FigureColoree fg) {
		nb_points_cliques=0;
		points_cliques = new Model.Point[fg.nbPoints()];
		figure=fg;
	}

	
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Methode qui construit une figure lorsque l utilisateur clique sur la souris
	 */
	public void mousePressed(MouseEvent e) {
		if(e.getButton()==MouseEvent.BUTTON1){
			if(nb_points_cliques<points_cliques.length-1) {
	            points_cliques[nb_points_cliques]=new Model.Point(e.getX(),e.getY());
	            nb_points_cliques++;
			}else {
				points_cliques[nb_points_cliques]=new Model.Point(e.getX(),e.getY());
				figure.modifierPoints(points_cliques);
				DessinModele dm = ((VueDessin)(e.getSource())).getDessin();
				dm.ajoute(figure);
			}
        }
	}
	
	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

}
