package Model;
import java.awt.*;

public abstract class Polygone extends FigureColoree{
	
	/**
	 * Attribut
	 * poly Polygon
	 */
	private Polygon poly;
	
	/**
	 * Constructeur
	 */
	public Polygone() {
		super();
		poly=new Polygon();
	}
	
	/**
	 * Getter de l attribut poly
	 * @return Polygon
	 */
	public Polygon getPoly() {
		return poly;
	}
	
	/**
	 * Methode qui affiche le polygone
	 * @param g Graphics
	 */
	public void affiche(Graphics g) {
		g.setColor(this.getCouleur());
		g.fillPolygon(poly);
	}
	
	/**
	 * Methode qui permet de translater le polygone
	 * @param x1 
	 * @param y1
	 */
	public void translation(int x1, int y1) {
		poly.translate(x1, y1);
	}
	
	/**
	 * Methode qui retourne le nombre de clics utiles a la construction du polygone
	 * @return int 
	 */
	public int nbClics() {
		return 4;
	}
	
	/**
	 * Methode qui sert a savoir si la souris est situee a l interieur du polygone
	 * @param a abscisse du point de la souris
	 * @param b ordonee du point de la souris
	 * @return boolean 
	 */
	public boolean estDedans(int a, int b) {
		return poly.contains(a,b);
	}
	
	//Methode abstraite
	public abstract void modifierPoints(Point[] pt);
	
}

