import javax.swing.*;

import java.awt.*;
public class PanneauChoix extends JPanel {


	private static final long serialVersionUID = 1L;
	private final static Color[] choixCouleur = {Color.red, Color.blue, Color.green};
	
	
	public PanneauChoix(VueDessin vd) {
		JRadioButton nv= new JRadioButton("Nouvelle Figure");
		JRadioButton tml= new JRadioButton("Trace a main levee");
		JRadioButton man= new JRadioButton("Manipulations");
		nv.setPreferredSize(new Dimension(200,50));
		this.add(nv);
		tml.setPreferredSize(new Dimension(200,50));
		this.add(tml);
		man.setPreferredSize(new Dimension(200,50));
		this.add(man);
	}
	
	public static Color determinerCouleur(int c) {
		return choixCouleur[c];
	}
	
	
}
