package Model;

public class Triangle extends Polygone{

	/**
	 * Constructeur
	 */
	public Triangle() {
		super();
	}
	
	/**
	 * Methode qui retourne le nombre de points pour construire un triangle
	 * @return int
	 */
	public int nbPoints() {
		return 3;
	}

	/**
	 * Methode qui modifie les points du triangle
	 * @param pt nouveau tableau de points du polygone et donc de la figure
	 */
	public void modifierPoints(Point[] pt) {
		for(int i =0; i<this.nbPoints(); i++) {
			this.getPoly().addPoint(pt[i].getX(),pt[i].getY());
			this.getP().add(new Model.Point(pt[i].getX(),pt[i].getY()));
		}
		
	}


}
