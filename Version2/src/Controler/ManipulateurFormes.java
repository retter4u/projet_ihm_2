package Controler;

import java.awt.event.*;
import java.util.*;

import javax.swing.SwingUtilities;

import Model.*;
import Vue.VueDessin;

public class ManipulateurFormes implements MouseListener, MouseMotionListener{

	/**
	 * Attributs
	 * last_X, last_Y coordonnees precedentes de la souris
	 * indiceSel indice de la figure selectionnee
	 * trans indique s il faut translater la figure ou non
	 * d DessinModele dans lequel s effectuent les modifications liees a la translation d une figure
	 * lfg liste de figures dans laquelle une figure est translatee
	 */
	private  int last_X, last_Y;
	private int indiceSel;
	private boolean trans;
	private DessinModele d;
	private ArrayList<FigureColoree> lfg;
	
	/**
	 * Constructeur
	 * @param dm DessinModele qui va etre manipule
	 */
	public ManipulateurFormes(DessinModele dm){
		if(dm!=null) {
			this.d=dm;
		}
		last_X=-1;
		last_Y=-1;
		this.lfg=d.getLfc();
		this.indiceSel=-1;
	}
	
	
	/**
	 * Methode qui selectionne ou deselectionne une figure grace a un clic de souris
	 */
	public void mouseClicked(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)){
			int i =0;
			boolean stop = false;
			while((i<this.lfg.size())&&(!stop)) {
				if(((Polygone)lfg.get(i)).estDedans(e.getX(),e.getY())) {
					if(indiceSel!=-1) {
						lfg.get(indiceSel).deselectionne();
					}
					lfg.get(i).selectionne();
					this.indiceSel=i;
					stop=true;
				}
				i++;
			}
		}
		if(SwingUtilities.isRightMouseButton(e)){
			if(indiceSel!=-1) {
				lfg.get(indiceSel).deselectionne();
				indiceSel=-1;
			}
		}
		if(SwingUtilities.isMiddleMouseButton(e)){
			
		}
		d.majAffichage();
	}

	/**
	 * Methode qui recupere la position de la souris lorsque le clic droit est utilise
	 */
	public void mousePressed(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)) {
			last_X=e.getX();
			last_Y=e.getY();
		}
		
	}

	/**
	 * Methode qui stop la translation de la figure lorsque le clic de souris est relache
	 */
	public void mouseReleased(MouseEvent e) {
		trans=false;
	}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

	/**
	 * Methode qui permet la translation de la figure
	 */
	public void mouseDragged(MouseEvent e) {
		if(indiceSel!=-1) {
			FigureColoree fc = lfg.get(indiceSel);
			int x = e.getX();
			int y = e.getY();
			if(!trans) {
				if (fc.estDedans(x, y)){
					fc.translation(x-last_X, y-last_Y);
				}
			}
			last_X=x;
			last_Y=y;
			d.majAffichage();
			}
	}

	public void mouseMoved(MouseEvent e) {}
	
	/**
	 * Methode qui retourne la figure selectionnee
	 * @return FigureColoree figure selectionnee
	 */
	public FigureColoree figureSelection() {
		FigureColoree fg;
		if(indiceSel!=-1) {
			fg = lfg.get(indiceSel);
		}else {
			fg =null;
		}
		return fg;
	}
	
	/**
	 * Methode qui retourne l indice de la figure selectionnee
	 * @return int indice de la figure selectionnee
	 */
	public int indiceFigureSelection() {
		return indiceSel+1;
	}
	
}
