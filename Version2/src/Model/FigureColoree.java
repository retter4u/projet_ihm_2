package Model;
import java.awt.*;
import java.util.ArrayList;

public abstract class FigureColoree{
	
	/**
	 * Attributs
	 * selected booleen qui sert a savoir si la figure est selectionnee ou non
	 * couleur Couleur de la figure
	 * tab_pt tableau de points de la figure
	 */
	private boolean selected;
	private Color couleur;
	private ArrayList<Model.Point> tab_pt;
	
	/**
	 * Constructeur
	 */
	public FigureColoree(){
		this.selected=false;
		this.couleur=Color.black;
		this.tab_pt= new ArrayList<Model.Point>();
	}
	
	/**
	 * Getter de l attribut tab_pt
	 * @return ArrayList<Model.Point>
	 */
	public ArrayList<Model.Point> getP() {
		return tab_pt;
	}
	
	/**
	 * Getter de l attribut couleur
	 * @return Color
	 */
	public Color getCouleur() {
		return couleur;
	}

	/**
	 * Setter de l attribut couleur
	 * @param couleur nouvelle couleur de la figure
	 */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	
	/**
	 * Methode qui selectionne la figure
	 */
	public void selectionne() {
		selected=true;
	}
	
	/**
	 * Methode qui deselectionne la figure 
	 */
	public void deselectionne() {
		selected=false;
	}
	
	//Methodes abstraites
	public abstract int nbPoints();
	
	public abstract int nbClics();
	
	public abstract boolean estDedans(int a, int b);
	
	public abstract void modifierPoints(Point[] pt);

	public abstract void affiche(Graphics g);
	
	public abstract void translation(int x1, int y1);	

	
}