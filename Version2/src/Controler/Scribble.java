package Controler;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import Model.DessinModele;
import Model.Trait;
import Vue.VueDessin;

	
	public class Scribble implements MouseListener, MouseMotionListener{

		/**
		 * Attributs
		 * dm DessinModele dans lequel les traits sont dessines
		 * lastX, lastY coordonnees precedentes de la souris
		 * couleur Color du trait dessine
		 * taille somme des traits effectues lors du dessin
		 */
		private DessinModele dm;
		private int lastX, lastY;
		private Color couleur;
		private int taille;
		
		
		/**
		 * Constructeur
		 * @param d dessinModele avec qui ce Controler interagit
		 */
		public Scribble(DessinModele d) {
			dm=d;
			this.lastX=-1;
			this.lastY=-1;
			this.couleur=Color.black;
			this.taille=0;
		}
		
		/**
		 * Setter de l attribut couleur
		 * @param c nouvelle couleur de l attribut couleur 
		 */
		public void setCouleur(Color c) {
			couleur=c;
		}

		/**
		 * Getter de l attribut taille
		 * @return int somme de tous les traits dessines
		 */
		public int getTaille() {
			return taille;
		}

		/**
		 * Methode qui dessine un trait en suivant le mouvement de la souris
		 */
		public void mouseDragged(MouseEvent e) {
			if(SwingUtilities.isLeftMouseButton(e)) {
				if(lastX!=-1 && lastY!=-1) {
					DessinModele dm = ((VueDessin)(e.getSource())).getDessin();
					Trait t = new Trait(lastX, lastY, e.getX(), e.getY(), couleur);
					taille++;
					dm.ajouteTrait(t);
					
				}
				lastX=e.getX();
				lastY=e.getY();
			}
			
		}
		
		/**
		 * Methode qui efface les traits si le clic du milieu est utilise
		 * ou recupere la position de la souris si le clic droit est utilise
		 */
		public void mousePressed(MouseEvent e) {
			if(SwingUtilities.isLeftMouseButton(e)) {
				lastX=e.getX();
				lastY=e.getY();
			}
			if(SwingUtilities.isMiddleMouseButton(e)) {
				effacer();
			}
		}

		public void mouseMoved(MouseEvent e) {}

		public void mouseClicked(MouseEvent e) {}

		public void mouseEntered(MouseEvent e) {}

		public void mouseExited(MouseEvent e) {}

		public void mouseReleased(MouseEvent e) {}
		
		/**
		 * Methode qui permet d effacer tous les traits dessines
		 */
		public void effacer() {
			for(int i=0; i<taille;i++) {
				dm.effTrait();
			}
		}
	}


