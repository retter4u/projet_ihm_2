package Model;

public class Point {

	/**
	 * Attributs
	 * x, y coordonnees du point
	 */
	private int x,y;
	
	/**
	 * Constructeur
	 * @param x1 abscisse du point
	 * @param y1 ordonnee du point
	 */
	public Point(int x1, int y1) {
		x=x1;
		y=y1;
	}
	
	/**
	 * Getter de l attribut x
	 * @return int
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Getter de l attribut y
	 * @return int
	 */
	public int getY() {
		return y;
	}

	/**
	 * Setter de l attribut x
	 * @param x nouvelle abscisse du point
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Setter de l attribut y
	 * @param y nouvelle ordonnee du point
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Methode pour incrementer d un certain nombre l abscisse du point
	 * @param x1 nombre ajoute a l abscisse
	 */
	public void incrementerX(int x1) {
		this.x += x1;
	}
	
	/**
	 * Methode pour incrementer d un certain nombre l ordonnee du point
	 * @param y1 nombre ajoute a l ordonnee
	 */
	public void incrementerY(int y1) {
		this.y += y1;
	}

}
