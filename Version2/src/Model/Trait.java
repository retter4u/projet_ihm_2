package Model;

import java.awt.Color;
import java.awt.Graphics;

public class Trait {

	/**
	 * Attributs
	 * couleur Couleur du trait
	 * x,y,x1,y1 coordonnes du trait
	 */
	private Color couleur;
	private int x,y,x1,y1;
	
	/**
	 * Constructeur
	 * @param dx abscisse du point de depart du trait 
	 * @param dy ordonnee du point de depart du trait 
	 * @param fx abscisse du point de fin du trait 
	 * @param fy ordonnee du point de fin du trait
	 * @param c couleur du trait
	 */
	public Trait(int dx, int dy, int fx, int fy, Color c) {
		couleur=Color.black;
		x=dx;
		y=dy;
		x1=fx;
		y1=fy;
		couleur = c;
	}

	/**
	 * Setter de l attribut x
	 * @param a nouvelle abscisse du point de depart du trait
	 */
	public void setX(int a) {
		this.x = a;
	}

	/**
	 * Setter de l attribut y
	 * @param b nouvelle ordonnee du point de depart du trait
	 */
	public void setY(int b) {
		this.y = b;
	}

	/**
	 * Setter de l attribut x1
	 * @param c nouvelle abscisse du point de fin du trait
	 */
	public void setX1(int c) {
		this.x1 = c;
	}

	/**
	 * Setter de l attribut y1
	 * @param d nouvelle ordonnee du point de fin du trait
	 */
	public void setY1(int d) {
		this.y1 = d;
	}
	
	/**
	 * Methode qui affiche le trait
	 * @param g
	 */
	public void affiche(Graphics g) {
		g.setColor(this.couleur);
		g.drawLine(x,y, x1, y1);
	}

	
	
}
