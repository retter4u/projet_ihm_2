package Controler;
import javax.swing.*;
import Vue.VueDessin;

import Model.*;

import java.awt.*;
import java.awt.event.*;

public class PanneauChoix extends JPanel {


	/**
	 * Attributs
	 * choixCouleur tableau des noms de couleurs disponibles
	 * choixFigure tableau des figures possibles a construire
	 * v VueDessin en relation avec ce Controler
	 * fc figure en cours de construction ou de manipulation
	 */
	private static final long serialVersionUID = 1L;
	private final static String[] choixCouleur = {"Rouge", "Bleu", "Vert", "Jaune", "Orange", "Gris"};
	private final static String[] choixFigure= {"Triangle", "Quadrilatere"};
	private VueDessin v;
	private FigureColoree fc;
	
	/**
	* Constructeur
	* @param vd VueDessin de l interface graphique
	*/
	public PanneauChoix(VueDessin vd) {
		
		this.setLayout(new GridLayout(2,3));
		
		//Initialisations de l attribut
		this.v=vd;
		
		//Creation des boutons
		ButtonGroup bg = new ButtonGroup();
		JRadioButton nv= new JRadioButton("Nouvelle Figure");
		JRadioButton tml= new JRadioButton("Trace a main levee");
		JRadioButton man= new JRadioButton("Manipulations");
		JComboBox jc = new JComboBox (choixFigure);
		JComboBox clr= new JComboBox(choixCouleur);
		nv.setBackground(Color.pink);
		tml.setBackground(Color.pink);
		man.setBackground(Color.pink);
		jc.setEnabled(false);
		clr.setEnabled(false);
		
		//Groupement des boutons
		bg.add(nv);
		bg.add(tml);
		bg.add(man);
		
		//Ajout des Listener
		ActionListener ml = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JRadioButton source = (JRadioButton)e.getSource();
				if(source==nv) {
					jc.setEnabled(true);
					clr.setEnabled(false);
					v.desactiveManipulationsSouris();
					v.desactiveScribble();
				}
				if(source == tml) {
					jc.setEnabled(false);
					clr.setEnabled(true);
					v.dessiner();
					v.desactiveManipulationsSouris();
					v.desactiveFabricant();
				}
				if (source==man){
					JOptionPane jop = new JOptionPane();
					jop.showMessageDialog(null, "Pour manipuler une figure, il faut la selectionner puis la bouger dans un second temps avec la souris ");
					jc.setEnabled(false);
					clr.setEnabled(true);
					v.desactiveScribble();
					v.desactiveFabricant();
					v.activeManipulationsSouris();
				}
			}
		};
		

		nv.addActionListener(ml);
		man.addActionListener(ml);
		tml.addActionListener(ml);
		this.add(nv);
		this.add(tml);
		this.add(man);
		

		jc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (jc.getSelectedIndex()) {
				case 0 : 
					fc = new Triangle();
					v.construit(fc);
					clr.setEnabled(true);
					break;
				case 1 :  
					fc = new Quadrilatere();
					clr.setEnabled(true);
					v.construit(fc);
					break;
				default: v.construit(new Quadrilatere());
							clr.setEnabled(true);
				}
			}
		});
		
		this.add(jc);
		this.add(clr);
		
		clr.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int res = clr.getSelectedIndex();
				Color c= determinerCouleur(res);
				if(nv.isSelected()) {
					fc.setCouleur(c);
					clr.setEnabled(false);
				}
				if(man.isSelected()) {
					if(v.figureSelection()!=null) {
						JOptionPane jop = new JOptionPane();
						int option = jop.showConfirmDialog(null, "Figure " +v.indiceSelection() + " selectionnee","Attention", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						if(option== JOptionPane.YES_OPTION) {v.figureSelection().setCouleur(c);}
					}else {
						JOptionPane jop = new JOptionPane();
						jop.showMessageDialog(null, "Aucune figure selectionnee","Attention", JOptionPane.WARNING_MESSAGE);
					}
					v.repaint();
				}
				if(tml.isSelected()) {
					v.getSc().setCouleur(c);
					v.repaint();
				}
			}
		});
		
		
		
		JButton jb = new JButton("EFFACER"); 
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] choix = new String[]{"Figure", "Trait", "Tout"};
				 
				int operateur = JOptionPane.showOptionDialog(null,"Que voulez-vous effacer ?",
					"Attention",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, choix, choix[1]);
				if(operateur==2) {
					v.getDessin().initDessinModele();
					v.supprimerAuditeurs();
					
				}
				if(operateur==1){
					for(int i=0; i<v.getSc().getTaille();i++) {
						v.getDessin().effTrait();
					}
					v.supprimerAuditeurs();
				}
				if(operateur==0){
					v.getDessin().effDessinModele();
					v.supprimerAuditeurs();
				}
			}
		});
		jb.setBackground(Color.red);
		this.add(jb);
	}
	
	
	
	/**
	 * M�thodequi d�termine la couleur du dessin selectionnee avec le jcombobox
	 * @param int index du bouton de couleur selectionne
	 * @return Color couleur determinee grace a l index
	 */
	public static Color determinerCouleur(int c) {
		Color col;
		switch (c) {
		case 0: col=Color.red;
		break;
		case 1: col=Color.blue;
		break;
		case 2 : col=Color.green;
		break;
		case 3 : col=Color.yellow;
		break;
		case 4 : col=Color.orange;
		break;
		case 5 : col=Color.gray;
		break;
		default: col= Color.black;
		}
	return col;
	
	}


	
}
