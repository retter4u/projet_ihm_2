package Vue;

import java.util.*;
import javax.swing.*;
import Controler.*;
import Model.*;

import java.awt.*;
public class VueDessin extends JPanel implements Observer{

	/**
	 * Attributs
	 * dm DessinModele avec qui cette Vue interagit
	 * ff, mf, sc Listeners qui interagissent avec cette Vue
	 */
	private static final long serialVersionUID = 1L;
	private DessinModele dm;
	private FabricantFigures ff;
	private ManipulateurFormes mf;
	private Scribble sc;
	
	/**
	 * Constructeur vide
	 */
	public VueDessin() {
		dm=new DessinModele();
		mf=new ManipulateurFormes(dm);
	}

	/**
	 * Getter de l attribut dm
	 * @return DessinModele
	 */
	public DessinModele getDessin() {
		return dm;
	}
	
	/**
	 * Geter de l attribut sc
	 * @return Scribble
	 */
	public Scribble getSc() {
		return sc;
	}

	/**
	 * Methode de mise a jour de la vue
	 * @param o
	 * @param arg
	 */
	public void update(Observable o, Object arg) {
		dm=(DessinModele)o;
		repaint();
	}
	
	/**
	 * Methode d'affichage de la vue
	 * @param g
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		dm.addObserver(this);
		if(dm.getLfc().size()!=0) {
			for(int i=0; i<dm.getLfc().size();i++) {
			dm.getLfc().get(i).affiche(g);
			}
		}
		if(dm.getT().size()!=0) {
			for(int j=0; j<dm.getT().size();j++) {
			dm.getT().get(j).affiche(g);
			}
		}
	}
	
	/**
	 * 	Methode de construction d une figure dans la vue
	 * @param figue FigureColoree que l utilisateut veut construire
	 */
	public void construit(FigureColoree figue) {
		ff = new FabricantFigures(figue);
		this.addMouseListener(ff);
	}
	
	/**
	 * Methode pour ajouter le Listener Scribble a la vue
	 */
	public void dessiner() {
		sc = new Scribble(dm);
		this.addMouseMotionListener(sc);
		this.addMouseListener(sc);
	}
	
	/**
	 * Methode pour activer le Listener ManipulationFormes
	 */
	public void activeManipulationsSouris() {
		this.addMouseListener(mf);
		this.addMouseMotionListener(mf);
	}
	
	/**
	 * Methode pour desactiver le Listener ManipulationFormes
	 */
	public void desactiveManipulationsSouris() {
		this.removeMouseListener(mf);
		this.removeMouseMotionListener(mf);
	}
	
	/**
	 * Methode pour desactiver le Listener Scribble
	 */
	public void desactiveScribble() {
		this.removeMouseListener(sc);
		this.removeMouseMotionListener(sc);
	}
	
	/**
	 * Methode pour desactiver le Listener FabricantFigures
	 */
	public void desactiveFabricant() {
		this.removeMouseListener(ff);
	}
	
	/**
	 * Methode pour desactiver tous les Listeners
	 */
	public void supprimerAuditeurs() {
		this.removeMouseListener(ff);
		this.removeMouseListener(mf);
		this.removeMouseMotionListener(mf);
		this.removeMouseListener(sc);
		this.removeMouseMotionListener(sc);
	}
	
	/**
	 * Methode qui retourne la figure selectionnee dans le DessinModele
	 * @return FigureColoree
	 */
	public FigureColoree figureSelection() {
		return mf.figureSelection();
	}
	
	/**
	 * Methode qui retourne l indice de la figure selectionnee dans le DessinModele
	 * @return int
	 */
	public int indiceSelection() {
		return mf.indiceFigureSelection();
	}
	
}