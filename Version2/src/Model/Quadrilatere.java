package Model;

public class Quadrilatere extends Polygone{
	
	/**
	 * Constructeur
	 */
	public Quadrilatere() {
		super();
	}
	
	/**
	 * Methode qui retourne le nombre de points pour construire un quadrilatere
	 * @return int
	 */
	public int nbPoints() {
		return 4;
	}

	/**
	 * Methode qui modifie les points du quadrilatere
	 * @param pt nouveau tableau de points du polygone et donc de la figure
	 */
	public void modifierPoints(Point[] pt) {
		for(int i =0; i<this.nbPoints(); i++) {
			this.getPoly().addPoint(pt[i].getX(),pt[i].getY());
			this.getP().add(new Model.Point(pt[i].getX(),pt[i].getY()));
		}
		
	}
	
}