package Model;
import java.awt.Color;
import java.util.*;


public class DessinModele extends Observable{
	
	/**
	 * Attributs
	 * lfc liste des figures de DessinModele
	 * t liste des Traits de DessinModele
	 */
	private ArrayList<FigureColoree> lfc;
	private ArrayList<Trait> t;

	/**
	 * Constructeur
	 */
	public DessinModele(){
		lfc=new ArrayList<FigureColoree>();
		t = new ArrayList<Trait>();
	}
	
	/**
	 * Getter de l attribut lfc
	 * @return ArrayList<FigureColoree>
	 */
	public ArrayList<FigureColoree> getLfc() {
		return lfc;
	}
	
	/**
	 * Getter de l attribut t
	 * @return ArrayList<Trait>
	 */
	public ArrayList<Trait> getT() {
		return t;
	}
	
	/**
	 * Methode qui ajoute une figure a la liste de figures
	 * @param fc FigureColoree ajoutee
	 */
	public void ajoute(FigureColoree fc) {
		lfc.add(fc);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Methode qui ajoute un trait a la liste de traits
	 * @param ta Trait ajoute
	 */
	public void ajouteTrait(Trait ta) {
		t.add(ta);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Methode qui reinitialise les deux listes
	 */
	public void initDessinModele() {
		for(int i =lfc.size()-1; i>=0;i--) {
			lfc.remove(i);
		}
		for(int j =t.size()-1; j>=0;j--) {
			t.remove(j);
		}
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Methode qui efface la derniere figure ajoutee
	 */
	public void effDessinModele() {
		lfc.remove(lfc.size()-1);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Methode qui efface le dernier trait ajoute
	 */
	public void effTrait() {
		t.remove(t.size()-1);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Methode qui met a jour l affichage du DessinModele
	 */
	public void majAffichage() {
		setChanged();
		notifyObservers();
	}
	
	
}